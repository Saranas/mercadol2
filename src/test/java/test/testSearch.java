package test ;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.Test;
import pages.homePage;

//public class testSearch extends config.functionalTest{
public class testSearch{
    
    private String product1 = "autos";
    
    @Test
    public void consulta(){
        System.setProperty("webdriver.gecko.driver", "src/main/resources/geckodriver.exe");
        WebDriver driver = new FirefoxDriver();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        driver.get("http://www.mercadolibre.cl");
        homePage homePage = new homePage(driver);
        homePage.busca(product1); 
        //aqui llamar al metodo que genere la lista.

    } 
    
}