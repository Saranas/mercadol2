package pages;

import config.PageObject;
import java.util.List;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;


public class homePage extends PageObject{
    
    @FindBy(name="as_word")
    private WebElement bar_search;
    
    @FindBy(id="nav_search_btn")
    private WebElement btn_search;
    
    @FindBy(id="results-section")
    private WebElement search_results; 
 
    public homePage(WebDriver driver) {
        super(driver);
    }
    
    public void busca(String texto){
       // this.bar_search.clear();
       this.bar_search.sendKeys(texto);
       this.bar_search.submit();
    }
       
    public void resultados(){
        String toString = this.search_results.toString();
        System.out.println("RESULTADOS");
        System.out.println(toString);
    }
    
    public void enviabusqueda(String palabra){
        this.btn_search.click();
    }
}
