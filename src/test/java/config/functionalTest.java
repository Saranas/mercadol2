package config;
import java.util.concurrent.TimeUnit;
import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class functionalTest {
        
        protected static WebDriver driver;           
       
        @Before
        public void setUp(){
            //System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");
            //driver = new ChromeDriver();
            System.setProperty("webdriver.gecko.driver", "src/main/resources/geckodriver.exe");
            WebDriver driver = new FirefoxDriver();
            driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
            driver.get("http://www.mercadolibre.cl");
        }
        @After
        public void cleanUp(){
            driver.manage().deleteAllCookies();
            driver.quit();
        }
}
